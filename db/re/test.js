// 1 -> ID
// 2 -> AegisName
// 3 -> Name
// 4 -> Type
// 5 -> Buy
// 6 -> Sell
// 7 -> Weight
// 8 -> ATK[:MATK]
// 9 -> DEF
// 10 -> Range
// 11 -> Slots
// 12 -> Job
// 13 -> Class
// 14 -> Gender
// 15 -> Loc
// 16 -> wLV
// 17 -> eLV[:maxLevel]
// 18 -> Refineable
// 19 -> View
// 20 -> { Script }
// 21 -> { OnEquip_Script }
// 22 -> { OnUnequip_Script }


.@r1 = getequiprefinerycnt(EQI_HEAD_TOP);
.@r2 = getequiprefinerycnt(EQI_ARMOR);
.@r3 = getequiprefinerycnt(EQI_HAND_L);
.@r4 = getequiprefinerycnt(EQI_HAND_R);
.@r5 = getequiprefinerycnt(EQI_GARMENT);
.@r6 = getequiprefinerycnt(EQI_SHOES);
.@r7 = getequiprefinerycnt(EQI_HEAD_MID);
.@r8 = getequiprefinerycnt(EQI_HEAD_LOW);
.@r9 = getequiprefinerycnt(EQI_COSTUME_HEAD_TOP);
.@r10 = getequiprefinerycnt(EQI_COSTUME_HEAD_MID);
.@r11 = getequiprefinerycnt(EQI_COSTUME_HEAD_LOW);
.@r12 = getequiprefinerycnt(EQI_COSTUME_GARMENT);
bonus bCritAtkRate, ((.@r1 + .@r2+ .@r3 + .@r4 + .@r5 + .@r6 + .@r7 + .@r8 + .@r9 + .@r10 + .@r11 + .@r12) * 5); 
bonus bShortWeaponDamageReturn, 500;
bonus bLongWeaponDamageReturn, 500;
bonus bMagicDamageReturn, 500;
bonus bAtkRate, (.@r1 + .@r2+ .@r3 + .@r4 + .@r5 + .@r6 + .@r7 + .@r8 + .@r9 + .@r10 + .@r11 + .@r12);
bonus bWeaponAtkRate, (.@r1 + .@r2+ .@r3 + .@r4 + .@r5 + .@r6 + .@r7 + .@r8 + .@r9 + .@r10 + .@r11 + .@r12);
bonus bSplashRange, 90;
bonus bSplashAddRange, 2; 


// EQI_ACC_L (0)             - Accessory 1
// EQI_ACC_R (1)             - Accessory 2
// EQI_SHOES (2)             - Footgear (shoes, boots)
// EQI_GARMENT (3)           - Garment (mufflers, hoods, manteaux)
// EQI_HEAD_LOW (4)          - Lower Headgear (beards, some masks)
// EQI_HEAD_MID (5)          - Middle Headgear (masks, glasses)
// EQI_HEAD_TOP (6)          - Upper Headgear
// EQI_ARMOR (7)             - Armor (jackets, robes)
// EQI_HAND_L (8)            - Left hand (weapons, shields)
// EQI_HAND_R (9)            - Right hand (weapons)
// EQI_COSTUME_HEAD_TOP (10) - Upper Costume Headgear
// EQI_COSTUME_HEAD_MID (11) - Middle Costume Headgear
// EQI_COSTUME_HEAD_LOW (12) - Lower Costume Headgear
// EQI_COSTUME_GARMENT (13)  - Costume Garment
// EQI_AMMO (14)    		  - Arrow/Ammunition
// EQI_SHADOW_ARMOR (15)     - Shadow Armor
// EQI_SHADOW_WEAPON (16)    - Shadow Weapon
// EQI_SHADOW_SHIELD (17)    - Shadow Shield
// EQI_SHADOW_SHOES (18)     - Shadow Shoes
// EQI_SHADOW_ACC_R (19)     - Shadow Accessory 2
// EQI_SHADOW_ACC_L (20)     - Shadow Accessory 1